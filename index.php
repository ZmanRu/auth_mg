<?php
include_once 'conf.php';
$auth = new auth();

//~ authorization
if (isset($_POST['send']) or isset($_POST['token']))
{
	if (!$auth->authorization())
	{
		$error = $auth->error_reporting();
	}
}

//~ user exit
$isAuth = $auth->check();
if (isset($_GET['exit'])) {
	$auth->exit_user();
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Учебники, задачники, решебники по математике, алгебре, геометрии и физике</title>
		<link type="text/css" href="/css/style.css" rel="stylesheet" />
		<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
</head>
<body>
	<div id="toppanel">
		<div id="panel">
			<div>
<?php
	if(!isset($_SESSION['id_user'])):
?>
				<div>		
<?php
			$k='';
			$auth = new auth(); 
			$k.='
	<table id="tdenter">
		<tr>
			<td>
			| <a href="enter.php" class="buttonenter">АВТОРИЗАЦИЯ</a> |
			<a href="join.php" class="buttonreg">РЕГИСТРАЦИЯ</a> |
			<a href="question.php" class="buttonquestion">ЗАДАТЬ ВОПРОС</a> |
			<a href="questions_answers.php" class="buttonquestanswer">ВОПРОСЫ И ОТВЕТЫ</a> |
			</td>
		</tr>
	</table>';
			print $k;
?>
				</div>
<?php	
	else:
?>
		<table id="tdexit">
			<tr>
				<td>
					|
			<a href="question.php" class="buttonquestion">ЗАДАТЬ ВОПРОС</a> |
			<a href="questions_answers.php" class="buttonquestanswer">ВОПРОСЫ И ОТВЕТЫ</a> |
			<a href="?exit" class="buttonexit">ВЫЙТИ</a> |
				</td>
			</tr>
		</table>		
<?php	
	endif;
?>
			</div>
		</div>
		<div class="tab">
			<ul class="login">
				<li>
<?php
	$r='';
	if ($isAuth) {
		$r.='Привет, '.$_SESSION['login_user'].'';
	} else {
		$r.='Привет, Гость';
	}
	if (isset($_SESSION['auth']) and $_SESSION['auth']=='ulog') {
		$data=mysql::query("SELECT * FROM `ulogin` WHERE `id_ulog`=".$_SESSION['id_user'].";", 'assoc');
		$r.='&nbsp;&nbsp;&nbsp;<img style="vertical-align:middle; width:28px; height:28px" src="'.$data['photo_ulog'].'" alt="" />';
	} elseif (isset($_SESSION['auth']) and $_SESSION['auth']!='ulog') {
			$data=mysql::query("SELECT * FROM `users` WHERE `id_user`=".$_SESSION['id_user'].";", 'assoc');
			if ($data['sex_user']=='1') {
				//~ Male
				$ava='/avatars/male_25x25_black.png';
			} else {
				//~ Female
				$ava='/avatars/female_25x25_black.png';
			}
			$r.='&nbsp;&nbsp;&nbsp;<img style="vertical-align:middle;" src="'.$ava.'" alt="" />';
	}
	print $r;
?>
				</li>
				<li id="toggle">
					<a id="open" class="open" href="#">
<?php
if (isset($_SESSION['id_user'])) {
	echo 'Открыть панель';
} else {
	echo 'Войти/Регистрация';
}

?>
					</a>	
					<a id="close" style="display: none;" class="close" href="#">Закрыть панель</a>
				</li>				
			</ul> 
		</div>
	</div>
<TABLE ID="tdupper" ALIGN="center" cellspacing="0">
</TABLE>
<TABLE CLASS="maintable" ALIGN="center" cellspacing="0"> 
	<TR> 
		<TD ID="tdleft">
		</TD> 
		<TD ID="tdcomments">
<?php
$comment = new comment;
//~ print comment
if ($auth->check()) {
	if (isset($_SESSION['com'])) {
		//~ print $_SESSION['com'];
		unset($_SESSION['com']);
	}
	if (isset($_POST['send_comment'])) {
		$back=$comment->add_comment();
		if ($back=='good') {
			$_SESSION['com']='<h2>You comment add to site</h2>';
			header("Location: ".$_SERVER["HTTP_REFERER"]);
		} else {
			print $back;
		}
	}
	print ''.$comment->add_form();
}
print $comment->view_comment();
?>
		</TD> 
	</TR> 
</TABLE>
</body>
</html>
