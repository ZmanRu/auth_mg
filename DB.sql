-- phpMyAdmin SQL Dump
-- version 3.5.0-rc2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Апр 01 2014 г., 18:44
-- Версия сервера: 5.5.35-0+wheezy1
-- Версия PHP: 5.4.4-14+deb7u8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `test_2`
--

-- --------------------------------------------------------

--
-- Структура таблицы `adm_file`
--

CREATE TABLE IF NOT EXISTS `adm_file` (
  `id_afile` int(5) NOT NULL AUTO_INCREMENT,
  `id_qus` int(5) NOT NULL,
  `path_afile` varchar(255) NOT NULL,
  PRIMARY KEY (`id_afile`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `class`
--

CREATE TABLE IF NOT EXISTS `class` (
  `id_cls` int(5) NOT NULL AUTO_INCREMENT,
  `name_cls` varchar(255) NOT NULL,
  PRIMARY KEY (`id_cls`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id_com` int(5) NOT NULL AUTO_INCREMENT,
  `id_usr` int(5) NOT NULL,
  `auth_type_com` int(1) NOT NULL,
  `date_com` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `text_com` text NOT NULL,
  PRIMARY KEY (`id_com`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `file`
--

CREATE TABLE IF NOT EXISTS `file` (
  `id_file` int(5) NOT NULL AUTO_INCREMENT,
  `id_qus` int(5) NOT NULL,
  `path_file` varchar(255) NOT NULL,
  PRIMARY KEY (`id_file`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `question`
--

CREATE TABLE IF NOT EXISTS `question` (
  `id_qus` int(5) NOT NULL AUTO_INCREMENT,
  `id_usr` int(5) NOT NULL,
  `id_thm` int(5) NOT NULL,
  `id_cls` int(5) NOT NULL,
  `auth_type_qus` int(1) NOT NULL,
  `status_qus` int(1) NOT NULL,
  `text_qus` text NOT NULL,
  `reply_qus` text NOT NULL,
  `data_qus` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_qus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `session`
--

CREATE TABLE IF NOT EXISTS `session` (
  `id_sess` int(5) NOT NULL AUTO_INCREMENT,
  `id_user` int(5) NOT NULL,
  `code_sess` varchar(15) NOT NULL,
  `user_agent_sess` varchar(255) NOT NULL,
  `date_sess` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `used_sess` int(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_sess`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `session_ulog`
--

CREATE TABLE IF NOT EXISTS `session_ulog` (
  `id_sessu` int(5) NOT NULL AUTO_INCREMENT,
  `id_ulog` int(5) NOT NULL,
  `code_sessu` varchar(255) NOT NULL,
  `user_agent_sessu` varchar(255) NOT NULL,
  `date_sessu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `used_sessu` int(10) NOT NULL,
  PRIMARY KEY (`id_sessu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `theme`
--

CREATE TABLE IF NOT EXISTS `theme` (
  `id_thm` int(5) NOT NULL AUTO_INCREMENT,
  `name_thm` varchar(255) NOT NULL,
  PRIMARY KEY (`id_thm`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `ulogin`
--

CREATE TABLE IF NOT EXISTS `ulogin` (
  `id_ulog` int(5) NOT NULL AUTO_INCREMENT,
  `login_ulog` varchar(255) NOT NULL,
  `identity_ulog` varchar(255) NOT NULL,
  `email_ulog` varchar(255) NOT NULL,
  `photo_ulog` varchar(255) NOT NULL,
  PRIMARY KEY (`id_ulog`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `ulogin`
--

INSERT INTO `ulogin` (`id_ulog`, `login_ulog`, `identity_ulog`, `email_ulog`, `photo_ulog`) VALUES
(1, 'Дмитрий', 'http://vk.com/id20140362', 'zekman-91@ya.ru', 'http://cs613424.vk.me/v613424362/66ca/ZgecrTAFPZQ.jpg'),
(2, 'Михаил', 'http://vk.com/id34144', 'mihon-epsilon@yandex.ru', 'http://cs309816.vk.me/v309816144/7599/uiN4jp4267E.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id_user` int(5) NOT NULL AUTO_INCREMENT,
  `login_user` varchar(60) NOT NULL,
  `passwd_user` varchar(255) NOT NULL,
  `mail_user` varchar(255) NOT NULL,
  `sex_user` int(1) NOT NULL,
  `key_user` varchar(10) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
