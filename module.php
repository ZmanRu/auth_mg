<?php

class mysql {
	/**
	*	Function connection to MySQL
	* @param	$db_host			domain or ip mysql server (localhost - default)
	* @param	$db_login			user name for connect to mysql server
	* @param	$db_password	password for conntect to mysql server
	* @param	$db_name			mysql database
	*/
	static function connect($db_host, $db_login, $db_passwd, $db_name) {
		mysql_connect($db_host, $db_login, $db_passwd) or die ("MySQL Error: " . mysql_error()); 
		mysql_query("set names utf8") or die ("<br>Invalid query: " . mysql_error());
		mysql_select_db($db_name) or die ("<br>Invalid query: " . mysql_error());
	}


	/**
	*	MySQL query
	*
	*	@param	$query		Mysql query
	*	@param	$type			Type: num_row, result, assoc or none
	*	@param	$num			Param in mysql_result
	*	@return	boolean		True or False
	*/
	static function query($query, $type=null, $num=null) {
		if ($q=mysql_query($query)) {
			switch ($type) {
				case 'num_row' : return mysql_num_rows($q); break;
				case 'result' : return mysql_result($q, $num); break;
				case 'assoc' : return mysql_fetch_assoc($q); break;
				case 'none' : return $q;
				default: return $q;
			}
		} else {
			return false;
		}
	}

	/**
	*	MySQL data screening
	*
	*	@param	$data			Screening string
	*	@return	string		Result string
	*/
	static function screening($data) {
		$data = trim($data);
		return mysql_real_escape_string($data);
	}

	/**
	 * MySQL data screening array
	 * @papam $data		screening array
	 * @return array	result array
	 */
	static function screening_array($data) {
		foreach ($data as $key=>$value) {
			$tmp[$key]=mysql::screening($value);
		}
		return $tmp;
	}
}


class admin {
	function check() {
		if ($_SESSION['auth']=='reg') {
			//~ example allowed user
			//~ switch($_SESSION['id_user']) {
				//~ case '1' : return true; break;
				//~ case '2' : return true; break;
				//~ default: return false;
			//~ }
		} elseif ($_SESSION['auth']='ulog') {
			switch($_SESSION['id_user']) {
				case '1' : return true; break;
				case '2' : return true; break;
				default: return false;
			}
		} else {
			return false;
		}
	}

	function count_new_comment() {
		return 0;
	}

	function new_question() {
		$question=new question;
		$count = $question->count_new_question();
		if ($count>0) {
			return $count. ' <a href="?new_question">view</a>';
		} else {
			return 'none';
		}
	}

	
}


class question {

	function get_theme() {
		$r='';
		$query=mysql::query("SELECT * FROM theme");
		while ($row=mysql_fetch_assoc($query)) {
			if (isset($_POST['theme']) and $_POST['theme']==$row['id_thm']) {
				$r.='<option value="'.$row['id_thm'].'" selected>'.$row['name_thm'].'</option>';
			} else {
				$r.='<option value="'.$row['id_thm'].'">'.$row['name_thm'].'</option>';
			}
		}
		return $r;
	}

	function get_class() {
		$r='';
		$query=mysql::query("SELECT * FROM class");
		while ($row=mysql_fetch_assoc($query)) {
			if (isset($_POST['class']) and $_POST['class']==$row['id_cls']) {
			$r.='<option value="'.$row['id_cls'].'" selected>'.$row['name_cls'].'</option>';
			} else {
				$r.='<option value="'.$row['id_cls'].'">'.$row['name_cls'].'</option>';
			}
		}
		return $r;
	}

	//~ form from new question
	function set_form($error=null) {
		$r=$error.'
			<form action="" method="post" enctype="multipart/form-data" >
				<div class="text_choose">
					Выберите предмет:
						<select name="theme" id="">
							'.$this->get_theme().'
						</select> и класс:
						<select name="class" id="">
							'.$this->get_class().'
						</select>
					<br />
				</div>
					<textarea name="text" id="" style="width:650px" rows="15">'.@$_POST['text'].'</textarea>
					<br />
					<table style="width:600px;">
						<tr>
						<td>
					<input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
					<input type="file" name="file" style="float:left;"/>
						</td>
						<td>
					<input type="submit" style="float:right;" value="Отправить вопрос" name="send_question" />
						</td>
						</tr>
						<tr>
						<td>
					<div class="text_input">(jpg, png, pdf, doc, docx, txt, zip), maximum 10 Mb</div>
					</td>
					</tr>
					</table>
			</form>
		';
		return $r;
	}

	//~ validate client form
	function validate_form() {
		//~ print 1;
		if (!$this->file_check()) {
			return '<h2>Error!</h2> File is not authenticated or its size is too large.';
		} elseif (mb_strlen($_POST['text'])<10) {
			return '<h2>Error!</h2> Your question is too short.';
		} else {
			return true;
		}
	}

	//~ save new question
	function save_new_question() {
		if ($_SESSION['auth']=='ulog') {
			$auth_type=1;
		} else {
			$auth_type=0;
		}
		$tmp_arr=mysql::screening_array($_POST);
		$text_question=strip_tags($tmp_arr['text']);
		$query=mysql::query("INSERT INTO `question` (`id_qus`, `id_usr`, `id_thm`, `id_cls`, `auth_type_qus`, `status_qus`, `text_qus`) VALUES (NULL, '".$_SESSION['id_user']."', '".$tmp_arr['theme']."', '".$tmp_arr['class']."', '".$auth_type."', '0', '".$tmp_arr['text']."');");
		if ($query) {
			$id_question=mysql_insert_id();
			if (mb_strlen($_FILES['file']['name'])>1) {
				//~ upload file
				$expansion = strtolower(pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION));
				$structure= 'files/'.$_SESSION['auth'].'/'.$_SESSION['id_user'].'/';
				if(is_dir($structure) or mkdir($structure, 0777, true)) {
					$target = $structure.md5($_FILES['file']['name'].date("M Y S")).'.'.$expansion;
					if(!move_uploaded_file($_FILES['file']['tmp_name'], $target)) {
						return 'Sorry you file not uploaded';
					} else {
						$query=mysql::query("INSERT INTO `file` (`id_file`, `id_qus`, `path_file`) VALUES (NULL, '".$id_question."', '/".$target."');");
						unset($_POST);
						//~ return 'Your question has been successfully sent to moderation.';
						return 'good';
					}
				} else {
					return '<h2>Sorry you file not uploaded</h2>';
				}
			} else {
				return 'good';
			}
		} else {
			return '<h2>Sorry you question is not add</h2>';
		}
	}


	//~ check uploaded file
	function file_check() {
		if (mb_strlen($_FILES['file']['name'])<1) {
			return true;
			die;
		}
		$expansion = strtolower(pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION));
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		$mime = finfo_file($finfo, $_FILES['file']['tmp_name']);

		$allow_expansion = array (
			"jpeg" => "image/jpeg",
			"jpg" => "image/jpeg",
			"jpe" => "image/jpeg",
			"png" => "image/png",
			"docx" => "application/msword",
			"doc" => "application/msword",
			"zip" => "application/zip",
			"pdf" => "application/pdf",
			"txt" => "text/plain",
		);

		if ($mime!==$_FILES['file']['type'] or $_FILES['file']['size']>10000000) {
			return false;
		} else {
			foreach ($allow_expansion as $key => $value) {
				if($value == $mime){
					if ($key == $expansion) {
						return true;
					}
				}
			}
			return false;
		}
	}
	
	function count_new_question() {
		return mysql::query("SELECT COUNT(*) FROM `question` WHERE `status_qus`=0", 'result', 0);
	}

	//~ list new question from admin panel
	function list_new_question() {
		$query=mysql::query("SELECT * FROM `question` INNER JOIN `theme` ON `theme`.`id_thm`=`question`.`id_thm` INNER JOIN `class` ON `class`.`id_cls`=`question`.`id_cls` WHERE `status_qus`=0");
		if (mysql_num_rows($query)==0) {
			return 'don\'t exists new questions. <a href="admin.php">return?</a>';
		}
		$template='
			<form action="" method="post" enctype="multipart/form-data">
				<input type="hidden" name="id_qus" value="{ID_QUS}" />
				theme and class: {THEME} <br />
				author: {AUTHOR} <br />
				Question: <br />
				<textarea name="question" cols="30" rows="10">{QUESTION}</textarea><br />
				Your reply: <br />
				<textarea name="reply" cols="30" rows="10"></textarea><br />
				{FILE}
				<br />
				<input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
				Only zip, txt, doc, docx, pdf, png and jpg file allowed. Max file size 10 MB.<br />
				<input type="file" name="file" id="" /><br />
				<input type="submit" value="save and reply" name="save" /><input type="submit" value="delete question" name="delete" />
			</form>
		';

		$r='';
		while ($row=mysql_fetch_assoc($query)) {
			if ($row['auth_type_qus']==1) {
				$tmp['AUTHOR']='ULOG USER - '.mysql::query("SELECT `login_ulog` FROM `ulogin` WHERE `id_ulog`=".$row['id_usr'].";", 'result', 0);
			} else {
				$tmp['AUTHOR']='REG USER - '.mysql::query("SELECT `login_user` FROM `users` WHERE `id_user`=".$row['id_usr'].";", 'result', 0);
			}
			$tmp['ID_QUS']=$row['id_qus'];
			$tmp['THEME']=$row['name_thm'].' '.$row['name_cls'];
			$tmp['QUESTION']=$row['text_qus'];
			$tmp['FILE']=$this->checkfile($row['id_qus']);
			$i=0; $tmp_r='';
			foreach($tmp as $key=>$value) {
				if ($i==0)
					$tmp_r=str_replace('{'.$key.'}', $value, $template);
				else
					$tmp_r=str_replace('{'.$key.'}', $value, $tmp_r);
				$i++;
			}
			$r.=$tmp_r.'<br /><hr /><br />';
		}
		return $r;
	}

	//~ check isset file
	function checkfile($id_qus) {
		$query=mysql::query("SELECT `path_file` FROM `file` WHERE `id_qus`=".$id_qus.";");
		if (mysql_num_rows($query)>0) {
			return 'Attached file: <a href="'.mysql_result($query, 0).'" target="_blank">download</a><br />';
		} else {
			return '';
		}
	}

	function checkRfile($id_qus) {
		$query=mysql::query("SELECT `path_afile` FROM `adm_file` WHERE `id_qus`=".$id_qus.";");
		if (mysql_num_rows($query)>0) {
			return 'Administrator attached file: <a href="'.mysql_result($query, 0).'" target="_blank">download</a><br />';
		} else {
			return '';
		}
	}


	//~ edit question from admin panel
	function update_question() {
		$data=mysql::screening_array($_POST);
		//~ add file
		if (mb_strlen($_FILES['file']['name'])>1) {
			//~ upload file
			$expansion = strtolower(pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION));
			$structure= 'files/'.$_SESSION['auth'].'/'.$_SESSION['id_user'].'/';
			if(is_dir($structure) or mkdir($structure, 0777, true)) {
				$target = $structure.md5($_FILES['file']['name'].date("M Y S")).'.'.$expansion;
				if(!move_uploaded_file($_FILES['file']['tmp_name'], $target)) {
					return 'Sorry you file not uploaded';
				} else {
					$query=mysql::query("INSERT INTO `adm_file` (`id_afile`, `id_qus`, `path_afile`) VALUES (NULL, '".$data['id_qus']."', '/".$target."');");
					$file=true;
				}
			} else {
				$file=false;
			}
			if (!$file) {
				return '<h2>Error upload file</h2>';
			}
		} 
		$query=mysql::query("UPDATE `question` SET `text_qus` = '".$data['question']."', `reply_qus` = '".$data['reply']."', `status_qus`=1 WHERE `question`.`id_qus` = ".$data['id_qus'].";");
		if ($query) {
			return '<h2>Question update</h2>';
		} else {
			return '<h2>Error update question</h2>';
		}
	}

	//~ delete question from admin panel
	function delete_question() {
		$r='';
		$query=mysql::query("DELETE FROM `question` WHERE `id_qus` = ".$_POST['id_qus'].";");
		if ($query) {
			$r.='<h2>Question removed</h2>';
			$query2=mysql::query("DELETE FROM `file` WHERE `id_qus` = ".$_POST['id_qus'].";");
			$file=mysql::query("SELECT `path_file` FROM `file` WHERE `id_qus`=".$_POST['id_qus'].";");
			if (mysql_num_rows($file)>0) {
				unlink(mysql_result($file, 0));
			}
			if ($query2) {
				$r.='<h2>File removed</h2>';
			} else {
				$r.='<h2>Error delete file</h2>';
			}
		} else {
			$r.='<h2>Error delete question</h2>';
		}
		return $r;
	}

	//~ search form
	function search_question() {
		return '
			<form action="/search.php" method="get">
				<input type="text" name="text" value="" required placeholder="search text" />
				<select name="theme" id="">
					<option value="везде">везде</option>
					'.$this->get_theme().'
				</select>
				<select name="class" id="">
					<option value="везде">везде</option>
					'.$this->get_class().'
				</select>
				<input type="submit" value="search" name="search" />
			</form>
		';
	}

	//~ form edit question from admin panel
	function edit_question_adm() {
		$query=mysql::query("SELECT * FROM `question` INNER JOIN `theme` ON `theme`.`id_thm`=`question`.`id_thm` INNER JOIN `class` ON `class`.`id_cls`=`question`.`id_cls` WHERE `id_qus`=".$_GET['edit_question'].";");
		if (mysql_num_rows($query)==0) {
			return 'don\'t exists questions. <a href="admin.php">return?</a>';
		}
		$template='
			<form action="" method="post"  >
				<input type="hidden" name="id_qus" value="{ID_QUS}" />
				theme and class: {THEME} <br />
				author: {AUTHOR} <br />
				Question: <br />
				<textarea name="question" cols="30" rows="10">{QUESTION}</textarea><br />
				Your reply: <br />
				<textarea name="reply" cols="30" rows="10">{REPLY}</textarea><br />
				{FILE}
				{RFILE}
				<input type="submit" value="update" name="save" /><input type="submit" value="delete question" name="delete" />
			</form>
		';

		$r='';
		while ($row=mysql_fetch_assoc($query)) {
			if ($row['auth_type_qus']==1) {
				$tmp['AUTHOR']='ULOG USER - '.mysql::query("SELECT `login_ulog` FROM `ulogin` WHERE `id_ulog`=".$row['id_usr'].";", 'result', 0);
			} else {
				$tmp['AUTHOR']='REG USER - '.mysql::query("SELECT `login_user` FROM `users` WHERE `id_user`=".$row['id_usr'].";", 'result', 0);
			}
			$tmp['ID_QUS']=$row['id_qus'];
			$tmp['THEME']=$row['name_thm'].' '.$row['name_cls'];
			$tmp['QUESTION']=$row['text_qus'];
			$tmp['REPLY']=$row['reply_qus'];
			$tmp['FILE']=$this->checkfile($row['id_qus']);
			$tmp['RFILE']=$this->checkRfile($row['id_qus']);
			$i=0; $tmp_r='';
			foreach($tmp as $key=>$value) {
				if ($i==0)
					$tmp_r=str_replace('{'.$key.'}', $value, $template);
				else
					$tmp_r=str_replace('{'.$key.'}', $value, $tmp_r);
				$i++;
			}
			$r.=$tmp_r.'<br /><hr /><br />';
		}
		return $r;
	}

	//~ user send question
	function set_question() {
		if (isset($_POST['send_question'])) {
			$validate=$this->validate_form();
			if ($validate===true) {
				//~ form valid
				return $this->save_new_question();
			} else {
				//~ form not valid
				return $validate.$this->set_form();
			}
		} else {
			return $this->set_form();
		}
	}

	//~ user list question
	function list_question() {
		$query=mysql::query("SELECT * FROM `question` INNER JOIN `theme` ON `theme`.`id_thm`=`question`.`id_thm` INNER JOIN `class` ON `class`.`id_cls`=`question`.`id_cls` WHERE `status_qus`=1");
		if (mysql_num_rows($query)==0) {
			return 'don\'t exists questions.';
		}
		$template='
			<img src="{AVATAR}" style="float:left;" /> {DATA} {NAME} <br /> {THEME} {CLASS} <br />
			<hr />
			<a href="?QUS={ID_QUS}">{QUESTION}</a>
		';
		$r='';
		while ($row=mysql_fetch_assoc($query)) {
			if ($row['auth_type_qus']==1) {
				$tmp_q=mysql::query("SELECT `login_ulog`, `photo_ulog` FROM `ulogin` WHERE `id_ulog`=".$row['id_usr'].";", 'assoc');
				$tmp['NAME']=$tmp_q['login_ulog'];
				$tmp['AVATAR']=$tmp_q['photo_ulog'];
			} else {
				$tmp_q=mysql::query("SELECT `login_user`, `sex_user` FROM `users` WHERE `id_user`=".$row['id_usr'].";", 'assoc');
				$tmp['NAME']=$tmp_q['login_user'];
				if ($tmp_q['sex_user']==1) {
					$tmp['AVATAR']='/avatars/male.png';;
				} else {
					$tmp['AVATAR']='/avatars/female.png';;
				}
			}
			$tmp['ID_QUS']=$row['id_qus'];
			$tmp['THEME']=$row['name_thm'];
			$tmp['CLASS']=$row['name_cls'];
			$tmp['DATA']=$this->convert_data($row['data_qus']);
			$tmp['QUESTION']=$row['text_qus'];
			$i=0; $tmp_r='';
			foreach($tmp as $key=>$value) {
				if ($i==0)
					$tmp_r=str_replace('{'.$key.'}', $value, $template);
				else
					$tmp_r=str_replace('{'.$key.'}', $value, $tmp_r);
				$i++;
			}
			$r.=$tmp_r.'<br /><hr /><br />';
		}
		return $r;
	}

	function convert_data($data) {
		$Y=mb_substr($data,0,4);
		$M=mb_substr($data,5,2);
		$D=mb_substr($data,8,2);
		$h=mb_substr($data,11,2);
		$m=mb_substr($data,14,2);
		return $D.'.'.$M.'.'.$Y.' '.$h.':'.$m;
	}

	//~ admin list question
	function list_admin_question() {
		$query=mysql::query("SELECT * FROM `question` INNER JOIN `theme` ON `theme`.`id_thm`=`question`.`id_thm` INNER JOIN `class` ON `class`.`id_cls`=`question`.`id_cls` WHERE `status_qus`=1");
		if (mysql_num_rows($query)==0) {
			return 'don\'t exists questions. <a href="/admin.php">return?</a>';
		}
		$template='
				theme and class: {THEME} <br />
				date and author: {DATE} {AUTHOR} <br />
				Question: <br />
				<a href="/admin.php?edit_question={ID_QUS}">{QUESTION}</a>
		';

		$r='';
		while ($row=mysql_fetch_assoc($query)) {
			if ($row['auth_type_qus']==1) {
				$tmp['AUTHOR']='ULOG USER - '.mysql::query("SELECT `login_ulog` FROM `ulogin` WHERE `id_ulog`=".$row['id_usr'].";", 'result', 0);
			} else {
				$tmp['AUTHOR']='REG USER - '.mysql::query("SELECT `login_user` FROM `users` WHERE `id_user`=".$row['id_usr'].";", 'result', 0);
			}
			$tmp['ID_QUS']=$row['id_qus'];
			$tmp['THEME']=$row['name_thm'].' '.$row['name_cls'];
			$tmp['QUESTION']=$row['text_qus'];
			$tmp['DATE']=$this->convert_data($row['data_qus']);
			$i=0; $tmp_r='';
			foreach($tmp as $key=>$value) {
				if ($i==0)
					$tmp_r=str_replace('{'.$key.'}', $value, $template);
				else
					$tmp_r=str_replace('{'.$key.'}', $value, $tmp_r);
				$i++;
			}
			$r.=$tmp_r.'<br /><hr /><br />';
		}
		return $r;
	}

	//~ user view question
	function view_question() {
		$id=mysql::screening($_GET['QUS']);
		if (!is_numeric($id)) {
			return '<h2>Invalid question number</h2>';
			die;
		}
		$query=mysql::query("SELECT * FROM `question` INNER JOIN `theme` ON `theme`.`id_thm`=`question`.`id_thm` INNER JOIN `class` ON `class`.`id_cls`=`question`.`id_cls` WHERE `id_qus`=".$id.";");
		if (mysql_num_rows($query)!=1) {
			return '<h2>Invalid question number</h2>';
			die;
		}

		$row=mysql_fetch_assoc($query);
		if ($row['auth_type_qus']==1) {
			$tmp_q=mysql::query("SELECT `login_ulog`, `photo_ulog` FROM `ulogin` WHERE `id_ulog`=".$row['id_usr'].";", 'assoc');
			$tmp['NAME']=$tmp_q['login_ulog'];
			$tmp['AVATAR']=$tmp_q['photo_ulog'];
		} else {
			$tmp_q=mysql::query("SELECT `login_user`, `sex_user` FROM `users` WHERE `id_user`=".$row['id_usr'].";", 'assoc');
			$tmp['NAME']=$tmp_q['login_user'];
			if ($tmp_q['sex_user']==1) {
				$tmp['AVATAR']='/avatars/male.png';;
			} else {
				$tmp['AVATAR']='/avatars/female.png';;
			}
		}
		$tmp['THEME']=$row['name_thm'];
		$tmp['CLASS']=$row['name_cls'];
		$tmp['DATA']=$this->convert_data($row['data_qus']);
		$tmp['QUESTION']=$row['text_qus'];
		$tmp['REPLY']=$row['reply_qus'];
		$tmp['FILE']=$this->checkfile($row['id_qus']);
		$tmp['RFILE']=$this->checkRfile($row['id_qus']);
		
		
		$template='
			<img src="{AVATAR}" style="float:left;" /> {DATA} {NAME} <br /> {THEME} {CLASS} <br />
			<hr />
			{QUESTION} <br />
			{FILE}
			<hr />
			{REPLY}
			<br />
			{RFILE}
			<br />
		';
		$i=0; $tmp_r='';
		foreach($tmp as $key=>$value) {
			if ($i==0)
				$tmp_r=str_replace('{'.$key.'}', $value, $template);
			else
				$tmp_r=str_replace('{'.$key.'}', $value, $tmp_r);
			$i++;
		}
		return $tmp_r;
	}

	//~ view result search in question
	function search_result($param) {
		$param=mysql::screening_array($param);
		$bad='<h2>bad search query</h2>';

		if ($param['theme']!='all') {
			if (!is_numeric($param['theme'])) {
				return $bad;
				die;
			} else {
				$qparam=" AND `id_thm`=".$param['theme'];
			}
		} else $qparam="";


		if ($param['class']!='all') {
			if (!is_numeric($param['class'])) {
				return $bad;
				die;
			} else {
				$qparam2=" AND `id_thm`=".$param['theme'];
			}
		} else $qparam2="";

		$qq="SELECT * FROM `question` INNER JOIN `theme` on `question`.`id_thm`=`theme`.`id_thm` INNER JOIN `class` ON `class`.`id_cls`=`question`.`id_cls` WHERE `status_qus`=1 AND ".$qparam.$qparam2." `text_qus` LIKE '%".$param['text']."%'";
		//~ var_dump($qq);
		$query=mysql::query($qq);
		if (!$query OR mysql_num_rows($query)==0) {
			return 'Nothing found';
			die;
		}

		$template='
			<img src="{AVATAR}" style="float:left;" /> {DATA} {NAME} <br /> {THEME} {CLASS} <br />
			<hr />
			<a href="/question.php?QUS={ID_QUS}">{QUESTION}</a>
		';
		$r='';
		while ($row=mysql_fetch_assoc($query)) {
			if ($row['auth_type_qus']==1) {
				$tmp_q=mysql::query("SELECT `login_ulog`, `photo_ulog` FROM `ulogin` WHERE `id_ulog`=".$row['id_usr'].";", 'assoc');
				$tmp['NAME']=$tmp_q['login_ulog'];
				$tmp['AVATAR']=$tmp_q['photo_ulog'];
			} else {
				$tmp_q=mysql::query("SELECT `login_user`, `sex_user` FROM `users` WHERE `id_user`=".$row['id_usr'].";", 'assoc');
				$tmp['NAME']=$tmp_q['login_user'];
				if ($tmp_q['sex_user']==1) {
					$tmp['AVATAR']='/avatars/male.png';;
				} else {
					$tmp['AVATAR']='/avatars/female.png';;
				}
			}
			$tmp['ID_QUS']=$row['id_qus'];
			$tmp['THEME']=$row['name_thm'];
			$tmp['CLASS']=$row['name_cls'];
			$tmp['DATA']=$this->convert_data($row['data_qus']);
			$tmp['QUESTION']=$row['text_qus'];
			$i=0; $tmp_r='';
			foreach($tmp as $key=>$value) {
				if ($i==0)
					$tmp_r=str_replace('{'.$key.'}', $value, $template);
				else
					$tmp_r=str_replace('{'.$key.'}', $value, $tmp_r);
				$i++;
			}
			$r.=$tmp_r.'<br /><hr /><br />';
		}
		return $r;
	}
}


class comment {

	function add_form() {
		return
		'<form action="" method="post" >
				<textarea style="margin-right:50px; overflow: auto;" name="text" id="" cols="35" rows="5" placeholder="ваш комментарий...">'.@$_POST['text'].'</textarea>
				<br />
				<input type="submit" style="margin-right:285px; value="send" name="send_comment" />
			</form>
			<br />
		';
	}

	function add_comment() {
		$tmp_data=mysql::screening_array($_POST);
		if (mb_strlen($tmp_data['text'])<1) {
			return '<h2>Your comment is too short</h2>';
			die;
		}
		if (mb_strlen($tmp_data['mail'])>0) {
			return '<h2>An unexpected error. sorry.</h2>';
			die;
		}
		
		if ($_SESSION['auth']=='ulog') {
			$auth_type=1;
		} else {
			$auth_type=0;
		}

		$conv_str=SwearFilter::filter($_POST['text']);

		$query=mysql::query("INSERT INTO `comment` (`id_com`, `id_usr`, `auth_type_com`, `date_com`, `text_com`) VALUES (NULL, '".$_SESSION['id_user']."', '".$auth_type."', CURRENT_TIMESTAMP, '".$conv_str."');");
		if ($query) {
			unset($_POST);
			//~ $_SESSION['COM']='<h2>You comment add to site</h2>';
			//~ return '<h2>You comment add to site</h2>';
			return 'good';
		} else {
			return '<h2>Sorry, your comment not add to site</h2>';
		}
	}

	function view_comment() {
		$query=mysql::query("SELECT * FROM `comment`  ORDER BY `date_com` DESC;");
		if (mysql_num_rows($query)<1) {
			return 'no comments';
			die;
		}
		$r='';
		$template='
		<div style="width: 300px; margin-right: 30px;">
<div>
			<img style="width:30px; height:30px; float:left;" src="{AVATAR}" />
</div>
		<div style="font-size:10px; margin-left: 40px;" align="left">
			{NAME}
			{DATA}
		</div>
		<div style="font-size:10px; padding: 3px 0px 0px 0px; margin-left: 40px; word-break: break-all;" align="left">
			{TEXT}
		</div>
		</div>';

		while ($row=mysql_fetch_assoc($query)) {
			if ($row['auth_type_com']==1) {
				$tmp_q=mysql::query("SELECT `login_ulog`, `photo_ulog` FROM `ulogin` WHERE `id_ulog`=".$row['id_usr'].";", 'assoc');
				$tmp['NAME']=$tmp_q['login_ulog'];
				$tmp['AVATAR']=$tmp_q['photo_ulog'];
			} else {
				$tmp_q=mysql::query("SELECT `login_user`, `sex_user` FROM `users` WHERE `id_user`=".$row['id_usr'].";", 'assoc');
				$tmp['NAME']=$tmp_q['login_user'];
				if ($tmp_q['sex_user']==1) {
					$tmp['AVATAR']='/avatars/male.png';;
				} else {
					$tmp['AVATAR']='/avatars/female.png';;
				}
			}
			$tmp['DATA']=$this->convert_data($row['date_com']);
			$tmp['TEXT']=$row['text_com'];

			$i=0; $tmp_r='';
			foreach($tmp as $key=>$value) {
				if ($i==0)
					$tmp_r=str_replace('{'.$key.'}', $value, $template);
				else
					$tmp_r=str_replace('{'.$key.'}', $value, $tmp_r);
				$i++;
			}
			$r.=$tmp_r.'<br />';
		}
		return $r;
	}


	function convert_data($data) {
		$Y=mb_substr($data,0,4);
		$M=mb_substr($data,5,2);
		$D=mb_substr($data,8,2);
		$h=mb_substr($data,11,2);
		$m=mb_substr($data,14,2);
		return $D.'.'.$M.'.'.$Y.' '.$h.':'.$m;
	}

}


class auth {
	static $error_arr=array();
	static $error='';
	
	/**
	 * This method validate user data
	 * @param		$login			user login
	 * @param		$passwd			user password one
	 * @param		$passwd2		user password two
	 * @param		$mail				user email
	 * @return	bollean			return true or false
	 */
	function check_new_user($login, $passwd, $passwd2, $mail) {
		//~ validate user data
		if (empty($login) or empty($passwd) or empty($passwd2)) $error[]='All fields are required';
		if ($passwd != $passwd2) $error[]='The passwords do not match';
		if (strlen($login)<3 or strlen($login)>30) $error[]='The login must be between 3 and 30 characters';
		if (strlen($passwd)<3 or strlen($passwd)>30) $error[]='The password must be between 3 and 30 characters';
		//~ validate email
		if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) $error[]='Not correct email';
		//~ Checks the user with the same name in the database
		if (mysql::query("SELECT * FROM users WHERE login_user='".$login."';", 'num_row')!=0) $error[]='A user with this name already exists';
		if (mysql::query("SELECT * FROM users WHERE mail_user='".$mail."';", 'num_row')!=0) $error[]='User with this email already exists';

		//~ return error array or TRUE
		if (isset($error)) {
			self::$error_arr=$error;
			return false;
		} else {
			return true;
		}
	}

	/**
	 *	This method is used to register a new user
	 *	@return	boolean or string			return true or html code error
	 */
	function reg() {
		//~ screening input data
		$tmp_arr=mysql::screening_array($_POST);
		$login=$tmp_arr['login'];
		$passwd=$tmp_arr['passwd'];
		$passwd2=$tmp_arr['passwd2'];
		$mail=$tmp_arr['mail'];
		//~ User floor translate to a numeric value
		if ($tmp_arr['sex']=='male') {
			$sex='1';
		} else {
			$sex='2';
		}
		//~ Check valid user data
		if ($this->check_new_user($login, $passwd, $passwd2, $mail)) {
			//~ User data is correct. Register.
			$user_key = $this->generateCode(10);
			$passwd = md5($user_key.$passwd.SECRET_KEY); //~ password hash with the private key and user key
			$query=mysql::query("INSERT INTO `users` (`id_user`, `login_user`, `passwd_user`, `mail_user`, `sex_user`, `key_user`) VALUES (NULL, '".$login."', '".$passwd."', '".$mail."', '".$sex."','".$user_key."');");
			if ($query) {
				return true;
			} else {
				self::$error='An error occurred while registering a new user. Contact the Administration.';
				return false;
			}
		} else {
			return false;
		}
	}


	/**
	 * This method checks whether the user is authorized
	 * @return		boolean				true or false
	 */
	function check() {
		if (isset($_SESSION['id_user']) and isset($_SESSION['login_user'])) {
			return true;
		} else {
			//~ Verify the existence of cookies
			if (isset($_COOKIE['id_user']) and isset($_COOKIE['code_user']) and isset($_COOKIE['auth'])) {
				switch($_COOKIE['auth']) {
					case 'reg' :
						$table="session";
						$table2="users";
						$pref="user";
						$pref2="sess";
						$ro='code_sess';
						$ro2='user_agent_sess';
						$ro3='id_sess';
					break;
					case 'ulog' :
						$table="session_ulog";
						$table2="ulogin";
						$pref="ulog";
						$pref2="sessu";
						$ro='code_sessu';
						$ro2='user_agent_sessu';
						$ro3='id_sessu';
					break;
					default: $this->destroy_cookie(); return false;
				}
				
				//~ cookies exist. Verified with a table sessions.
				$id_user=mysql::screening($_COOKIE['id_user']);
				$code_user=mysql::screening($_COOKIE['code_user']);
				$query=mysql::query("SELECT `".$table."`.*, `".$table2."`.`login_".$pref."` FROM `".$table."` INNER JOIN `".$table2."` ON `".$table2."`.`id_".$pref."`=`".$table."`.`id_".$pref."` WHERE `".$table."`.`id_".$pref."`=".$id_user.";");
				//~ var_dump("SELECT `".$table."`.*, `".$table2."`.`login_".$pref."` FROM `".$table."` INNER JOIN `".$table2."` ON `".$table2."`.`id_".$pref."`=`".$table."`.`id_".$pref."` WHERE `".$table."`.`id_".$pref."`=".$id_user.";"); die;
				if ($query and mysql_num_rows($query)!=0) {
					//~ Cookies are found in the database
					$user_agent=mysql::screening($_SERVER['HTTP_USER_AGENT']);
					while ($row=mysql_fetch_assoc($query)) {
						if ($row[$ro]==$code_user and $row[$ro2]==$user_agent) {
							//~ found record
							mysql::query("UPDATE `".$table."` SET `used_".$pref."` = `used_".$pref."`+1 WHERE `id_".$pref."` = ".$row[$ro3].";");
							//~ start session and update cookie
							$login='login_'.$pref;
							$id='id_'.$pref;
							$code='code_'.$pref2;
							$_SESSION['id_user']=$row[$id];
							$_SESSION['login_user']=$row[$login];
							$_SESSION['auth']=$_COOKIE['auth'];
							setcookie("id_user", $row[$id], time()+3600*24*30);
							setcookie("code_user", $row[$code], time()+3600*24*30);
							setcookie("auth", $_COOKIE['auth'], time()+3600*24*30);
							return true;
						}
					}
					//~ No records with this pair of matching cookies/user agent
					$this->destroy_cookie();
					return false;
				} else {
					//~ No records for this user
					$this->destroy_cookie();
					return false;
				}
			} else {
				//~ cookies nit exist
				$this->destroy_cookie();
				return false;
			}
		}
	}

	/**
	 * This method performs user authorization
	 * @return boolen			true or false
	 */
	function authorization() {
		//~ screening user data
		$user_data=mysql::screening_array($_POST);
		if (isset($_POST['token']))  {
			$s=file_get_contents('http://ulogin.ru/token.php?token='.$_POST['token'].'&host='.$_SERVER['HTTP_HOST']);
			$user=json_decode($s, true);
			$user=mysql::screening_array($user);
			$q=mysql::query("SELECT * FROM `ulogin` WHERE `identity_ulog`='".$user['identity']."';");
			if (mysql_num_rows($q)==1) {
				//~ found ulogin
				//~ start session and set cookie with found user
				$data=mysql_fetch_assoc($q);
				$_SESSION['id_user']=$data['id_ulog'];
				$_SESSION['login_user']=$data['login_ulog'];
				$_SESSION['auth']='ulog';
				$cook_code=$this->generateCode(15);
				$user_agent=mysql::screening($_SERVER['HTTP_USER_AGENT']);
				$tmp_q="INSERT INTO `session_ulog` (`id_sessu`, `id_ulog`, `code_sessu`, `user_agent_sessu`) VALUES (NULL, '".$data['id_ulog']."', '".$cook_code."', '".$user_agent."');";
				//~ print $tmp_q; die;
				mysql::query($tmp_q);
				setcookie("id_user", $_SESSION['id_user'], time()+3600*24*30);
				setcookie("code_user", $cook_code, time()+3600*24*30);
				setcookie("auth", 'ulog', time()+3600*24*30);
				header("Location: /");
				return true;
			} else {
				$q=mysql::query("INSERT INTO `ulogin` (`id_ulog`, `login_ulog`, `identity_ulog`, `email_ulog`, `photo_ulog`)
												VALUES (NULL, '".$user['first_name']."', '".$user['identity']."', '".$user['email']."', '".$user['photo']."');");
				if (!$q) { self::$error='Error adding new user. Contact with the administration.'; return false; }
				$id=mysql_insert_id();
				//~ start session and set cookie with new user
				$_SESSION['id_user']=$id;
				$_SESSION['login_user']=$user['first_name'];
				$_SESSION['auth']='ulog';
				$cook_code=$this->generateCode(15);
				$user_agent=mysql::screening($_SERVER['HTTP_USER_AGENT']);
				mysql::query("INSERT INTO `session_ulog` (`id_sessu`, `id_ulog`, `code_sessu`, `user_agent_sessu`) VALUES (NULL, '".$id."', '".$cook_code."', '".$user_agent."');");
				setcookie("id_user", $_SESSION['id_user'], time()+3600*24*30);
				setcookie("code_user", $cook_code, time()+3600*24*30);
				setcookie("auth", 'ulog', time()+3600*24*30);
				header("Location: /");
				return true;
			}
			self::$error='Error. Contact with the administration.';
			return false;
		}
		//~ Find a user with the same name and taking his key
		$find_user=mysql::query("SELECT * FROM `users` WHERE `login_user`='".$user_data['login']."';", 'assoc');
		if (!$find_user) {
			//~ user not found
			self::$error='User not found';
			return false;
		} else {
			//~ user found
			$passwd=md5($find_user['key_user'].$user_data['passwd'].SECRET_KEY); //~ password hash with the private key and user key
			if ($passwd==$find_user['passwd_user']) {
				//~ passwords match
				$_SESSION['id_user']=$find_user['id_user'];
				$_SESSION['login_user']=$find_user['login_user'];
				$_SESSION['auth']='reg';
				//~ if user select "remember me"
				if (isset($user_data['remember']) and $user_data['remember']=='on') {
					$cook_code=$this->generateCode(15);
					$user_agent=mysql::screening($_SERVER['HTTP_USER_AGENT']);
					mysql::query("INSERT INTO `session` (`id_sess`, `id_user`, `code_sess`, `user_agent_sess`) VALUES (NULL, '".$find_user['id_user']."', '".$cook_code."', '".$user_agent."');");
					setcookie("id_user", $_SESSION['id_user'], time()+3600*24*30);
					setcookie("code_user", $cook_code, time()+3600*24*30);
					setcookie("auth", 'reg', time()+3600*24*30);
				}
				header("Location: /");
				return true;
			} else {
				//~ passwords not match
				self::$error='User not found or password not match';
				return false;
			}
		}
	}

	/**
	 * This method is used for the user exit
	 */
	function exit_user() {
		//~ Destroy session, delete cookie and redirect to main page
		session_destroy();
		if (isset($_SERVER['HTTP_COOKIE'])) {
			$cookies = explode(';', $_SERVER['HTTP_COOKIE']);
			foreach($cookies as $cookie) {
					$parts = explode('=', $cookie);
					$name = trim($parts[0]);
					setcookie($name, '', time()-1000);
					setcookie($name, '', time()-1000, '/');
			}
		}
		header("Location: /");
	}

	/**
	 * This method destroy cookie
	 */
	function destroy_cookie() {
		if (isset($_SERVER['HTTP_COOKIE'])) {
			$cookies = explode(';', $_SERVER['HTTP_COOKIE']);
			foreach($cookies as $cookie) {
					$parts = explode('=', $cookie);
					$name = trim($parts[0]);
					setcookie($name, '', time()-1000);
					setcookie($name, '', time()-1000, '/');
			}
		}
	}

	/**
	 * This method is used for password recovery.
	 */
	function recovery_pass($login, $mail) {
		$login=mysql::screening($login);
		$mail=mysql::screening($mail);
		if (!filter_var($mail, FILTER_VALIDATE_EMAIL)){
			self::$error='Not correct email';
			return false;
		}
		//~ select data from this login
		$find_user = mysql::query("SELECT * FROM `users` WHERE `login_user`='".$login."';", 'assoc');
		if ($find_user) {
			if ($find_user['mail_user']!=$mail) {
				//~ Email does not meet this login.
				self::$error='Email does not conform to this login';
				return false;
			} else {
				//~ email and login is correct
				$new_passwd = $this->generateCode(8);
				$new_passwd_sql = md5($find_user['key_user'].$new_passwd.SECRET_KEY); //~ password hash with the private key and user key
				$message="You have requested a password recovery site sitename.\nYour new password: ".$new_passwd;
				if ($this->send_recovery_mail($find_user['mail_user'], $message)) {
					mysql::query("UPDATE `users` SET `passwd_user`='".$new_passwd_sql."' WHERE `id_user` = ".$find_user['id_user'].";");
					return true;
				} else {
					self::$error='A new password has been sent. Contact with the administration.';
					return false;
				}
			}
		} else {
			//~ this login - not found
			self::$error='User not found';
			return false;
		}
	}

	/**
	 * This method sends an email with a new password for that user.
	 * @return boolean				true or false
	 */
	function send_recovery_mail($mail,$message) {
		if (mail($mail, "Recovery password from site sitename", $message, "From: webmaster@sitename.ru\r\n"."Reply-To: webmaster@sitename.ru\r\n"."X-Mailer: PHP/" . phpversion())) {
			return true;
		} else {
			return false;
		}
	}

 	/**
 	 *	This method generate random string
 	 * @param		$length				int - length string
 	 * @return	string				result random string
 	 */
	function generateCode($length) { 
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789"; 
		$code = ""; 
		$clen = strlen($chars) - 1;   
		while (strlen($code) < $length) { 
			$code .= $chars[mt_rand(0,$clen)];   
		} 
		return $code; 
	}

	/**
	 *	This method returns the current error
	 */
	function error_reporting() {
		$r='';
		if (mb_strlen(self::$error)>0) {
			$r.=self::$error;
		}
		if (count(self::$error_arr)>0) {
			$r.='<h2>The following errors occurred:</h2>'."\n".'<ul>';
			foreach(self::$error_arr as $key=>$value) {
				$r.='<li>'.$value.'</li>';
			}
			$r.='</ul>';
		}
		return $r;
	}
}


/**
 * $Id:$
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * For more information, see
 * <http://www.tvxgames.ru>.
 */

/**
 * SwearFilter is a helper type class for filer swears in input strings. 
 * <example>
 *     $newString = SwearFilter::filter($string);
 * </example>
 * 
 * This class based on principles of Anti-Mat class writen by Sarov Pro Gamers 
 * (http://spg.arbse.net, admin_progamers@rambler.ru). You can download original class via link 
 * http://spg.arbse.net/index/anti_mate_v0,1_upd.rar. Main difference from origin - better filtering 
 * and transliteration, much size of filter words array, ~2,5 faster.
 *   
 * @category   Core
 * @package    String
 * @version    SVN: $Id$
 * @author     Nikolay Bondarenko <n.bondarenko@tvxgames.ru>
 * @copyright  2003-2010 TVXGames
 * @license    http://www.gnu.org/licenses/lgpl.txt LGPL License 3.0
 * @link       http://tvxgames.ru/
 * @see        Location  
 */
class SwearFilter 
{
    /**
     * Hold transliteration table (english set)
     * 
     * @var array
     */
    static private $translitTableFrom = array(
        'a', 'b', 'v', 'g', 'd', 'e', 'g', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p',
        'r', 's', 't', 'u', 'f', 'i', 'e', 'A', 'B', 'V', 'G', 'D', 'E', 'G', 'Z', 'I',
        'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'I', 'E', 'yo', 'h',
        'ts', 'ch', 'sh', 'shch', 'yu', 'ya', 'YO', 'H', 'TS', 'CH', 'SH', 'SHCH', 'YU', 'YA'
    );

    /**
     * Hold transliteration table (russian set)
     * 
     * @var array
     */
    static private $translitTableTo = array(
        'а', 'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п',
        'р', 'с', 'т', 'у', 'ф', 'ы', 'э', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И',
        'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Ы', 'Э', 'ё', 'х',
        'ц', 'ч', 'ш', 'щ', 'ю', 'я', 'Ё', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ю', 'Я'
    );
    
    /**
     * Bad words array of string in preg_match regexp's compatibile format. You must manualy assign 
     * regexp's delimiters, modifier for matching string start/end. Also don`t forget to use PCRE8
     * modifier (http://ru2.php.net/manual/en/reference.pcre.pattern.modifiers.php).
     *    
     * @var array 
     */
    static private $badWords = array(
        "/^.*ху(й|и|я|е|л(и|е)).*/u", "/^.*пи(з|с)д.*/u", "/^бля.*/u", "/^.*бля(д|т|ц).*/u", 
        "/^(с|сц)ук(а|о|и).*/u", "/^еб.*/u", "/^.*уеб.*/u", "/^заеб.*/u", "/^.*еб(а|и)(н|с|щ|ц).*/u", 
        "/^.*ебу(ч|щ).*/u", "/^.*п(и|ы)д(о|е)р.*/u", "/^.*хер.*/u", "/^г(а|о)ндон/u", "/^.*залуп.*/u"
    );
    
    /**
     * Use it to filtrate input string for swear words. During filtration all swear words replaced
     * by '*' symbol. 
     *
     * @param string $string Source string
     * @return string Filtered string with swear replaced by '*' symbol.
     */
    public function filter($string)
    {
        $swearingFound = false; 
        //here we explode string to words
        $elems = explode (" ", $string); 
        $count_elems = count($elems);
        
        for ($i = 0; $i < $count_elems; $i++) {
            //Transliterate alnum filterter string. We can`t use strtr due to UTF8.
            $str_rep = str_replace(self::$translitTableFrom,self::$translitTableTo,
                preg_replace('/[^a-zA-Zа-яА-Яё]/u', '', mb_strtolower($elems[$i], 'UTF8'))
            );
                        
            //Here we are trying to find bad word matching in the special array
            $countBadWords = count(self::$badWords);
            for ($k = 0; $k < $countBadWords; $k++) {
                if (preg_match(self::$badWords[$k], $str_rep)) {
                    $elems[$i] = str_repeat('*', mb_strlen($elems[$i], 'UTF8') - 1);
                    $swearingFound = true;
                    break;
                }
            }
        }
        
        //here we implode words in the whole string
        if ($swearingFound) {
            $string = implode (" ", $elems);
        }
        
        return $string;
    }
}

?>
