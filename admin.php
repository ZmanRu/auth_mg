<?php
include_once 'conf.php';
$r='';

$auth = new auth;
$question = new question;
$admin = new admin;

//~ user exit
if (isset($_GET['exit'])) {
	$auth->exit_user();
}

//~ Check auth
if (!$auth->check() or !$admin->check()) {
	$auth->exit_user();
}

if (isset($_GET['edit_question'])) {
	if (isset($_POST['save'])) {
		print $question->update_question();
	} elseif(isset($_POST['delete'])) {
		print $question->delete_question();
	} else {
		print $question->edit_question_adm();
	}
} elseif (isset($_GET['list_question'])) {
	print $question->list_admin_question();
} elseif(isset($_GET['new_question'])) {
	if (isset($_POST['save'])) {
		print $question->update_question();
	} elseif (isset($_POST['delete'])) {
		print $question->delete_question();
	}
	print $question->list_new_question();
} else {
	//~ get count new question and comment
	print 'new question: '.$admin->new_question().'<br />';
	print '<a href="/admin.php?list_question">edit question</a>';
}

?>
