<?php
include_once 'conf.php';
$auth = new auth();

//~ authorization
if (isset($_POST['send']) or isset($_POST['token']))
{
	if (!$auth->authorization())
	{
		$error = $auth->error_reporting();
	}
}

//~ user exit
$isAuth = $auth->check();
if (isset($_GET['exit']))
	{
	$auth->exit_user();
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Учебники, задачники, решебники по математике, алгебре, геометрии и физике</title>
		<link type="text/css" href="/css/style.css" rel="stylesheet" />
		<link type="text/css" href="/css/menu.css" rel="stylesheet" />
		<link type="text/css" href="/css/join.css" rel="stylesheet" />
		<link type="text/css" href="/css/enter.css" rel="stylesheet" />
		<link rel="stylesheet" type="text/css" href="/login_panel/css/slide.css" media="screen" />
		<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
		<script src="/login_panel/js/slide.js" type="text/javascript"></script>
		<script type="text/javascript" src="/js/tinymce/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init({    
        
        language : "ru",
        // General options
        mode : "textareas",
        theme : "advanced",
        plugins : "emotions,print,fullscreen",

        // Theme options
        theme_advanced_buttons1 : "undo,redo,|,bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,outdent,indent,|,print,|,fullscreen",
        theme_advanced_buttons2 : "fontselect,fontsizeselect,|,forecolor,backcolor,|,sub,sup,|,charmap,emotions",
		theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_resizing : true,

        // Skin options
        skin : "default",
        skin_variant : "silver"        
});
</script>
</head>
<body>
	<div id="toppanel">
		<div id="panel">
			<div>
<?php
	if(isset($_SESSION['id_user'])):
?>
				<div>		
<?php
			$k='';
			$auth = new auth(); 
			$k.='
	<table id="tdenter">
		<tr>
			<td>
			| <a href="enter.php" class="buttonenter">АВТОРИЗАЦИЯ</a> |
			<a href="join.php" class="buttonreg">РЕГИСТРАЦИЯ</a> |
			<a href="question.php" class="buttonquestion">ЗАДАТЬ ВОПРОС</a> |
			<a href="questions_answers.php" class="buttonquestanswer">ВОПРОСЫ И ОТВЕТЫ</a> |
			</td>
		</tr>
	</table>';
			print $k;
?>
				</div>
<?php	
	else:
?>
		<table id="tdexit">
			<tr>
				<td>
					|
			<a href="question.php" class="buttonquestion">ЗАДАТЬ ВОПРОС</a> |
			<a href="questions_answers.php" class="buttonquestanswer">ВОПРОСЫ И ОТВЕТЫ</a> |
			<a href="?exit" class="buttonexit">ВЫЙТИ</a> |
				</td>
			</tr>
		</table>		
<?php	
	endif;
?>
			</div>
		</div>
		<div class="tab">
			<ul class="login">
				<li>
<?php
	$r='';
	if ($isAuth) {
		$r.='Привет, '.$_SESSION['login_user'].'';
	} else {
			$r.='Привет, Гость';
		}
	if (isset($_SESSION['auth']) and $_SESSION['auth']=='ulog') {
		$data=mysql::query("SELECT * FROM `ulogin` WHERE `id_ulog`=".$_SESSION['id_user'].";", 'assoc');
		$r.='&nbsp;&nbsp;&nbsp;<img style="vertical-align:middle; width:28px; height:28px" src="'.$data['photo_ulog'].'" alt="" />';
	} elseif (isset($_SESSION['auth']) and $_SESSION['auth']!='ulog') {
			$data=mysql::query("SELECT * FROM `users` WHERE `id_user`=".$_SESSION['id_user'].";", 'assoc');
			if ($data['sex_user']=='1') 
			{
				//~ Male
				$ava='/avatars/male_25x25_black.png';
			} 
			else 
			{
				//~ Female
				$ava='/avatars/female_25x25_black.png';
			}
			$r.='&nbsp;&nbsp;&nbsp;<img style="vertical-align:middle;" src="'.$ava.'" alt="" />';
	}
	print $r;
?>
				</li>
				<li id="toggle">
					<a id="open" class="open" href="#">
<?php 
if (isset($_SESSION['id_user'])) {
	echo 'Открыть панель';
} else {
	echo 'Войти/Регистрация';
}
?>
					</a>	
					<a id="close" style="display: none;" class="close" href="#">Закрыть панель</a>
				</li>				
			</ul> 
		</div>
	</div>
<TABLE ID="tdupper" ALIGN="center" cellspacing="0">
</TABLE>
<TABLE CLASS="maintable" ALIGN="center" cellspacing="0"> 
	<TR> 
		<TD ID="tdleft"> 
<?php
//~  этой директории вообще в принципе нету
//~ require_once($_SERVER['DOCUMENT_ROOT'].'/content/left.php');
?>
		</TD> 
		<TD ID="tdright">
			<div align="center" class="text_tdright">ЗАДАЙТЕ ВОПРОС ПО МАТЕМАТИКЕ ИЛИ ФИЗИКЕ</div>
<?php
$question = new question;

//~ add question form
if (!isset($_GET['QUS'])) 
{
	if (isset($_SESSION['qus'])) 
	{
		print $_SESSION['qus'];
		unset($_SESSION['qus']);
	}
	
	if ($auth->check()) 
	{
		$reply=$question->set_question();
		if ($reply=='good') 
		{
			$_SESSION['qus']='<div class="thanks">
									Спасибо за Ваш вопрос! Смотрите ответ в рубрике "Вопросы и ответы".
							</div>';
			header("Location: ".$_SERVER["HTTP_REFERER"]);
		} 
		else 
		{
			print $reply;
		}
	}
}
else 
{
	print '<hr />'.$question->view_question();
}
?>
		</TD> 
	</TR> 
</TABLE>
<TABLE CLASS="maintable" ALIGN="center" cellspacing="1"> 
	<TR> 
		<TD ID="tdbottom">
			Footer - счетчики, авторские права и т.п. 
		</TD> 
	</TR> 
</TABLE> 
</body>
</html>
